﻿using System;
using System.Drawing;
using static System.Math;

namespace Task_2
{
    public class Rectangle
    {
        public Rectangle(Point a, Point c)
        {
            if ((a == null) || (c == null))
            {
                throw new NullReferenceException();
            }

            if (a == c)
            {
                throw new Exception("Points of Rectangle must have a different coordinates");
            }

            A = a;
            C = c;

            B = new Point(A.X + Width, A.Y);
            D = new Point(c.X - Width, c.Y);
        }

        public int Width => Abs(A.X - C.X);
        public int Height => Abs(A.Y - C.Y);
        public int Perimeter => (Width + Height) * 2;
        public int Area => Width * Height;

        public Point A { get; private set; }
        public Point B { get; private set; }
        public Point C { get; private set; }
        public Point D { get; private set; }

        public static Rectangle Union(Rectangle firstRectangle, Rectangle secondRectangle)
        {
            int firstRectangleMinSide = Min(firstRectangle.Width, firstRectangle.Height);
            int secondRectangleMinSide = Min(secondRectangle.Width, secondRectangle.Height);
            int firstRectangleMaxSide = Max(firstRectangle.Width, firstRectangle.Height);
            int secondRectangleMaxSide = Max(secondRectangle.Width, secondRectangle.Height);

            int hight = firstRectangleMinSide + secondRectangleMinSide;
            int width = Max(firstRectangleMaxSide, secondRectangleMaxSide);

            return new Rectangle(new Point(0, hight), new Point(width, 0));
        }

        public static Rectangle Intersect(Rectangle firstRectangle, Rectangle secondRectangle)
        {
            int aX = Max(firstRectangle.A.X, secondRectangle.A.X);
            int aY = Min(firstRectangle.A.Y, secondRectangle.A.Y);
            int cX = Min(firstRectangle.C.X, secondRectangle.C.X);
            int cY = Max(firstRectangle.C.Y, secondRectangle.C.Y);

            return new Rectangle(new Point(aX, aY), new Point(cX, cY));
        }

        public Rectangle Union(Rectangle rectangle) => Union(this, rectangle);

        public Rectangle Intersect(Rectangle rectangle) => Intersect(this, rectangle);

        public void HightChange(int hight)
        {
            A = new Point(A.X, A.Y + hight);
            B = new Point(B.X, B.Y + hight);
        }

        public void WidthChange(int widht)
        {
            B = new Point(B.X + widht, B.Y);
            C = new Point(C.X + widht, C.Y);
        }

        public void ScaleChange(int scale)
        {
            A = new Point(A.X, A.Y * scale);
            B = new Point(B.X * scale, B.Y * scale);
            C = new Point(C.X * scale, C.Y);
        }

        public void XMove(int step)
        {
            A = new Point(A.X + step, A.Y);
            B = new Point(B.X + step, B.Y);
            C = new Point(C.X + step, C.Y);
            D = new Point(D.X + step, D.Y);
        }

        public void YMove(int step)
        {
            A = new Point(A.X, A.Y + step);
            B = new Point(B.X, B.Y + step);
            C = new Point(C.X, C.Y + step);
            D = new Point(D.X, D.Y + step);
        }

        public override string ToString()
        {
            return $"\t A({A.X},{A.Y}) B({B.X},{B.Y}) C({C.X},{C.Y}) D({D.X},{D.Y})";
        }
    }
}
