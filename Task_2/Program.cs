﻿using System.Drawing;
using static System.Console;

namespace Task_2
{
    internal class Demo
    {
        static void Main()
        {
            var rectangle1 = new Rectangle(new Point(0, 3), new Point(4, 1));
            var rectangle2 = new Rectangle(new Point(0, 4), new Point(4, 1));

            WriteLine("Rectangle Start Position:" + rectangle1);

            rectangle1.XMove(2);
            WriteLine("Rectangle After XMove(2):" + rectangle1);

            rectangle1.YMove(2);
            WriteLine("Rectangle After YMove(2):" + rectangle1);

            rectangle1.HightChange(3);
            WriteLine("Rectangle HightChange(3):" + rectangle1);

            rectangle1.WidthChange(3);
            WriteLine("Rectangle WidthChange(3):" + rectangle1);

            rectangle1.XMove(-2);
            WriteLine("Rectangle After XMove(-2):" + rectangle1);

            rectangle1.YMove(-2);
            WriteLine("Rectangle After YMove(-2):" + rectangle1);

            rectangle1.HightChange(-3);
            WriteLine("Rectangle HightChange(-3):" + rectangle1);

            rectangle1.WidthChange(-3);
            WriteLine("Rectangle WidthChange(-3):" + rectangle1);

            rectangle1.ScaleChange(2);
            WriteLine("Rectangle ScaleChange(2): " + rectangle1);

            WriteLine(new string('-', 70));

            WriteLine("Rectangle 1 position:" + rectangle1);
            WriteLine("Rectangle 2 position:" + rectangle2);

            WriteLine(new string('-', 70));

            var rectangle3 = rectangle1.Union(rectangle2);
            WriteLine("Union of rectangle1 & rectangle2 :" + rectangle3);

            var rectangle4 = rectangle1.Intersect(rectangle2);
            WriteLine("Intersect of rectangle1 & rectangle2 :" + rectangle4);

            ReadKey();
        }
    }
}
